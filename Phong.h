#ifndef __Phong__
#define __Phong__

#include "Material.h"
#include "Lambertian.h"
#include "GlossySpecular.h"

//----------------------------------------------------------------------------- class Phong

class Phong: public Material {	
	public:
			
		Phong(void);											

		Phong(const Phong& m);
		
		virtual Material*										
		clone(void) const;									

		Phong& 
		operator= (const Phong& rhs);							

		~Phong(void);											
		
		// These are to set the ka and kd of the Lambertians
		void 													
		set_ka(const float k);
		
		void 													
		set_kd(const float k);
		
		// These are to set cd of the Lambertians
		void													
		set_cd(const RGBColor c);
		
		void													
		set_cd(const float r, const float g, const float b);
		
		void																						
		set_cd(const float c);
				

		// These are for the GlossySpecular, sets the ks and exp of the GlossySpecular
		void
		set_ks(const float k);

		void
		set_exp(const float e);

		
		virtual RGBColor										
		shade(ShadeRec& sr);
		
		void 
		set_sampler(Sampler* sPtr);
		
	private:		
		SmartPointer<Lambertian>		ambient_brdf;
		SmartPointer<Lambertian>		diffuse_brdf;
		SmartPointer<GlossySpecular>	specular_brdf;	// Glossyspecular's cs is white for phong by default
};


// ---------------------------------------------------------------- set_ka
// this sets Lambertian::kd
// there is no Lambertian::ka data member because ambient reflection 
// is diffuse reflection

inline void								
Phong::set_ka(const float k) {
	ambient_brdf->set_kd(k);
}


// ---------------------------------------------------------------- set_kd
// this also sets Lambertian::kd, but for a different Lambertian object

inline void								
Phong::set_kd (const float k) {
	diffuse_brdf->set_kd(k);
}


// ---------------------------------------------------------------- set_cd

inline void												
Phong::set_cd(const RGBColor c) {
	ambient_brdf->set_cd(c);
	diffuse_brdf->set_cd(c);
	specular_brdf->set_cs(c);
}


// ---------------------------------------------------------------- set_cd

inline void													
Phong::set_cd(const float r, const float g, const float b) {
	ambient_brdf->set_cd(r, g, b);
	diffuse_brdf->set_cd(r, g, b);
}

// ---------------------------------------------------------------- set_cd

inline void													
Phong::set_cd(const float c) {
	ambient_brdf->set_cd(c);
	diffuse_brdf->set_cd(c);
}

// ---------------------------------------------------------- set_sampler

inline void
Phong::set_sampler(Sampler* sPtr) {
	ambient_brdf->set_sampler(sPtr);
	diffuse_brdf->set_sampler(sPtr);
}

// ---------------------------------------------------------- set_ks

inline void
Phong::set_ks(const float k) {
	specular_brdf->set_ks(k);
}

// ---------------------------------------------------------- set_exp

inline void
Phong::set_exp(const float e) {
	specular_brdf->set_exp(e);
}
#endif