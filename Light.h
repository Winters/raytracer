#ifndef __LIGHT__
#define __LIGHT__

#include "Vector3D.h"
#include "RGBColor.h"
#include "Ray.h"

#include "ReferenceCount.h"
#include "SmartPointer.h"

class ShadeRec;


//-------------------------------------------------------------------- class Light

class Light : public ReferenceCount {	
	public:
	
		Light(void);
								
		Light(const Light& ls);			

		Light& 								
		operator= (const Light& rhs); 

		virtual Light* 						
		clone(void) const = 0;
		
		virtual 							
		~Light(void);
						
		virtual Vector3D								
		get_direction(ShadeRec& sr) = 0;				
																
		virtual RGBColor														
		L(ShadeRec& sr);		

		// For shadow
		void
		set_shadows(bool enabled);	// Returns whether the light should cast a shadow or not.

		// For shadow
		bool
		casts_shadow() const;	// Returns whether the light should cast a shadow or not.

		virtual bool
		in_shadow(const Ray& ray, const ShadeRec& sr) const;	// Calculates whether the current object is in the shadow

	protected:
		bool shadow_enabled;
};

// set shadow
inline void
Light::set_shadows(bool enabled)
{
	shadow_enabled = enabled;
}

#endif