#ifndef __PointLight__
#define __PointLight__

#include "Light.h"
#include "Vector3D.h"
#include "RGBColor.h"

#include "World.h"			// you will need this later on for shadows
#include "ShadeRec.h"

// Creates a pointlight at given location. Location defaults to 0, 1, 0.
class PointLight: public Light {
	public:
	
		PointLight(void);   							

		PointLight(const PointLight& dl); 
		
		virtual Light* 									
		clone(void) const;			

		PointLight& 									
		operator= (const PointLight& rhs); 
			
		virtual											
		~PointLight(void); 
				
		void
		scale_radiance(const float b);
		
		void
		set_color(const float c);
		
		void
		set_color(const RGBColor& c);
		
		void
		set_color(const float r, const float g, const float b); 		
			
		void
		set_location(Point3D d);	// Set location 					
		
		void
		set_location(float dx, float dy, float dz);	// Set location
		
		virtual Vector3D								
		get_direction(ShadeRec& sr);
				
		virtual RGBColor		
		L(ShadeRec& sr);	
		
		// For shadow

		virtual bool
		in_shadow(const Ray& ray, const ShadeRec& sr) const;	// Calculates whether the current object is in the shadow
	private:

		float		ls;			
		RGBColor	color;
		Point3D		loc;		// locection the light comes from
};


// inlined access functions


// ------------------------------------------------------------------------------- scale_radiance

inline void
PointLight::scale_radiance(const float b) { 
	ls = b;
}

// ------------------------------------------------------------------------------- set_color

inline void
PointLight::set_color(const float c) {
	color.r = c; color.g = c; color.b = c;
}


// ------------------------------------------------------------------------------- set_color

inline void
PointLight::set_color(const RGBColor& c) {
	color = c;
}


// ------------------------------------------------------------------------------- set_color

inline void
PointLight::set_color(const float r, const float g, const float b) {
	color.r = r; color.g = g; color.b = b;
}


// ---------------------------------------------------------------------- set_location

inline void
PointLight::set_location(Point3D d) {
	loc = d;							// Set the location
}


// ---------------------------------------------------------------------- set_location 

inline void
PointLight::set_location(float dx, float dy, float dz) {
	loc.x = dx; loc.y = dy; loc.z = dz;	// Set the location
}


#endif
