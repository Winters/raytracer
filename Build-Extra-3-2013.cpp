// Extra image. Circles arranged in a spiral in a cutout of a box.
// Directional ligh shining down and a point light located in the spiral

void 												
World::build(void) {
	int num_samples = 16; // use 9 or 16 samples for production image
	  
	vp.set_hres(600);
	vp.set_vres(600);
	vp.set_pixel_size(0.5);
	vp.set_samples(num_samples);  
	
	background_color = black;
	tracer_ptr = new RayCast(this);
	
	Pinhole* pinhole_ptr = new Pinhole;
	pinhole_ptr->set_eye(-400.0, 1000, 400.0); 
	pinhole_ptr->set_lookat(0.0, 70.0, 0.0);
	pinhole_ptr->set_view_distance(1000.0);
	pinhole_ptr->compute_uvw();     
	set_camera(pinhole_ptr);
	
	// lights
	Ambient* ambient_ptr = new Ambient();
	ambient_ptr->scale_radiance(0.2);
	set_ambient_light(ambient_ptr);
	
	Directional* directional_ptr = new Directional;
	directional_ptr->set_direction(0, 100, 100);
	directional_ptr->scale_radiance(2.0);
	directional_ptr->set_shadows(true); 				
	add_light(directional_ptr);
	
	PointLight* point_light_ptr = new PointLight;
	point_light_ptr->set_location(0, 120, 0);
	point_light_ptr->scale_radiance(3.0);
	point_light_ptr->set_shadows(true); 						
	add_light(point_light_ptr);
	
	// Common parameters for all Phong materials
	float ka 	= 0.25;
	float kd 	= 0.65;

	const double frequency = PI * 2 / (TWO_PI * 20);
	
	// Create the spiral
	for (int t = 0; t < TWO_PI * 20; t++)
	{
		float x = t * cos(6*(double)t);
		float y = t * sin(6*(double)t);
		float z = t ;

		// Add rainbow color
		double red   = sin(frequency * (double)t + (double)2) * (double)127 + (double)128;
		double green = sin(frequency * (double)t + (double)0) * (double)127 + (double)128;
		double blue  = sin(frequency * (double)t + (double)4) * (double)127 + (double)128;
		
		RGBColor color(red / 255, green / 255, blue / 255);
		Matte* ptr = new Matte;
		ptr->set_ka(ka);	
		ptr->set_kd(kd);
		ptr->set_cd(color);	
			
		int factor = t != 0 ? t / 10: 1 / 10 ;
		Sphere*	sphere_ptr_1 = new Sphere(Point3D(x, z, y), 1.1 * factor); 
		sphere_ptr_1->set_material(ptr);	   							
		add_object(sphere_ptr_1);
	}

	// Create the box
	RGBColor gray(0.6);											

	Matte* matte_ptr = new Matte;
	matte_ptr->set_ka(0.25);	
	matte_ptr->set_kd(0.65);
	matte_ptr->set_cd(gray);
	Plane* plane_ptr = new Plane(Point3D(0.0, -100.0, 0.0), Normal(0.0, 1.0, 0.0));
	
	plane_ptr->set_material(matte_ptr);
	add_object(plane_ptr);

	Matte* matte_ptr2 = new Matte;
	matte_ptr2->set_ka(0.15);	
	matte_ptr2->set_kd(0.75);
	matte_ptr2->set_cd(gray);
	Plane* plane_ptr2 = new Plane(Point3D(0.0, 0.0, -300.0), Normal(0.0, 0.0, 1.0));
	
	plane_ptr2->set_material(matte_ptr2);
	add_object(plane_ptr2);

	Matte* matte_ptr3 = new Matte;
	matte_ptr3->set_ka(0.15);	
	matte_ptr3->set_kd(0.75);
	matte_ptr3->set_cd(gray);
	Plane* plane_ptr3 = new Plane(Point3D(300.0, 0.0, 0.0), Normal(-1.0, 0.0, 0.0));
	
	plane_ptr3->set_material(matte_ptr3);
	add_object(plane_ptr3);
}