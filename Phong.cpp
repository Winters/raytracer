#include "Phong.h"

// ---------------------------------------------------------------- default constructor

Phong::Phong (void)
	:	Material(),
		ambient_brdf(new Lambertian),
		diffuse_brdf(new Lambertian),
		specular_brdf(new GlossySpecular)
{}



// ---------------------------------------------------------------- copy constructor

Phong::Phong(const Phong& m)
	: 	Material(m)
{
	if(m.ambient_brdf)
		ambient_brdf = m.ambient_brdf->clone(); 
	
	if(m.diffuse_brdf)
		diffuse_brdf = m.diffuse_brdf->clone();

	if(m.specular_brdf)
		specular_brdf = m.specular_brdf->clone();
}


// ---------------------------------------------------------------- clone

Material*										
Phong::clone(void) const {
	return (new Phong(*this));
}	


// ---------------------------------------------------------------- assignment operator

Phong& 
Phong::operator= (const Phong& rhs) {
	if (this == &rhs)
		return (*this);
		
	Material::operator=(rhs);
	
	if (rhs.ambient_brdf)
		ambient_brdf = rhs.ambient_brdf->clone();
		
	if (rhs.specular_brdf)
		specular_brdf = rhs.specular_brdf->clone();

	if (rhs.diffuse_brdf)
		diffuse_brdf = rhs.diffuse_brdf->clone();

	return (*this);
}


// ---------------------------------------------------------------- destructor

Phong::~Phong(void) {}


// ---------------------------------------------------------------- shade

RGBColor
Phong::shade(ShadeRec& sr) {
	Vector3D 	wo 			= -sr.ray.d;
	RGBColor 	L 			= ambient_brdf->rho(sr, wo) * sr.w.ambient_ptr->L(sr);
	int 		num_lights	= sr.w.lights.size();
	
	for (int j = 0; j < num_lights; j++) {
		Vector3D wi(sr.w.lights[j]->get_direction(sr));    
		float ndotwi = sr.normal * wi;
		float ndotwo = sr.normal * wo;
	
		if (ndotwi > 0.0 && ndotwo > 0.0) {
			// This is the same as Matte except with specular added.
			bool in_shadow = false;

			// Checks whether the object is lying in the shadow at this point
			if (sr.w.lights[j]->casts_shadow()) {
				Ray shadow_ray(sr.hit_point, wi);
				in_shadow = sr.w.lights[j]->in_shadow(shadow_ray, sr);
			}

			// If it's not in the shadow, add lighting on top of the existing ambient light (which doesn't cast a shadow).

			if (!in_shadow) {
				L += (diffuse_brdf->f(sr, wo, wi) + specular_brdf->f(sr, wo, wi)) * sr.w.lights[j]->L(sr) * ndotwi;
			}
		}
	}
	
	return (L);
}



