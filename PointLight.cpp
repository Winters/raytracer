#include "PointLight.h"

// ---------------------------------------------------------------------- default constructor

PointLight::PointLight(void)
	: 	Light(),
		ls(1.0),
		color(1.0),
		loc(0, 1, 0)			
{}


// ---------------------------------------------------------------------- dopy constructor

PointLight::PointLight(const PointLight& dl)
	: 	Light(dl),
		ls(dl.ls),
		color(dl.color),
		loc(dl.loc)  		
{}


// ---------------------------------------------------------------------- clone

Light* 
PointLight::clone(void) const {
	return (new PointLight(*this));
}


// ---------------------------------------------------------------------- assignment operator

PointLight& 
PointLight::operator= (const PointLight& rhs) 	
{
	if (this == &rhs)
		return (*this);
			
	Light::operator= (rhs);
	
	ls		= rhs.ls;
	color 	= rhs.color;
	loc 	= rhs.loc;

	return (*this);
}


// ---------------------------------------------------------------------- destructor																			

PointLight::~PointLight(void) {}


// ---------------------------------------------------------------------- get_direction
// as this function is virtual, it shouldn't be inlined 

Vector3D								
PointLight::get_direction(ShadeRec& sr) {
	return ((loc - sr.hit_point).hat());	// The direction is calculated based on the the pointlight location and the hitpoint.
}	

// ------------------------------------------------------------------------------  L

RGBColor
PointLight::L(ShadeRec& s) {	
	return (ls * color);
}

bool
PointLight::in_shadow(const Ray& ray, const ShadeRec& sr) const {
	double t;
	double d = loc.distance(ray.o);
	int num_objects = sr.w.objects.size();

	for (int i = 0; i < num_objects; i++)
	{
		if (sr.w.objects[i]->shadow_hit(ray, t) && t < d)
		{
			return true;
		}
	}

	return false;
}